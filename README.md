README #

Proceso para generar las facturas respectivas 

### Primero conectarse servidor ITP ###

	* Ejecutar comando php por dia para el tiempo que se desea correr la facturación
		*php /var/Applications/htdocs/yachay/admy_img_carga.php 20201107
		*ejemplo de carga por dia ( 2020 año,11 mes y 07 dia).

### Segundo ejecutar proceso desde interfaz ###

	*Telefonía Fija:
	*Poner fecha emisión: 05-0x-201x
	*Limpiar TMP
	*Cargar
	*Calcular Datos
	*Bolsa
	*Pasar a Facturación
	*Imprimir
	*Tener en cuenta que los correlativos no estan funcionando haciendo que se repita los FYVZ y BYVZ
	*En el caso de imprimir, para TF, se divide en 2 o 3 bloques
	*Por el tema de llamadas de dynamicall (su detalle es superior a 10 hojas)
	*o Nos dirigimos al PHP: admy_rep_fact_tfnia.php función “ImprimirFacturas” modificamos
	*Linea 380
	
	*Ahí finaliza todo el proceso de facturación preselección
	*Se envia los pdf generados
	*Ruta /var/Applications/htdocs/yachay/archivos/tfn_facturas

### Tener en cuenta ###

	*Evitar generar correlativos duplicados,debido a que el proceso no esta terminado por parte de la facturación electrónica.
	*Remitir via email los pdf generados a telefonica para su validción.
	* Ejecutar querys adjuntas para poder terminar el proceso
